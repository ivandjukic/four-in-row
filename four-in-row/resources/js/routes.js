import HomeComponent from './components/HomeComponent.vue';
import StartNewGameForm from './components/StartNewGameForm.vue';
import GameComponent from './components/GameComponent.vue';
import RankingComponent from './components/RankingComponent.vue';
import PlayerComponent from './components/PlayerComponent.vue';

export const routes = [
    { 
        path: '/', 
        component: HomeComponent, 
        name: 'Home' 
    },
    { 
        path: '/new_game', 
        component: StartNewGameForm, 
        name: 'StartNewGameForm' 
    },
    { 
        path: '/game', 
        component: GameComponent, 
        name: 'Game' 
    },
    { 
        path: '/rankings', 
        component: RankingComponent, 
        name: 'Ranking' 
    },
    { 
        path: '/player', 
        component: PlayerComponent, 
        name: 'Player' 
    }
];
