<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/newgame', 'GameController@startNew');
Route::post('/savegameresults', 'GameController@saveMatchResults');
Route::get('/rankings', 'RankingsController@getRankings');
Route::get('/playerdata', 'PlayerController@getPlayerData');