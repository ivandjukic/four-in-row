<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Game extends Authenticatable
{
    use Notifiable;

    protected $table = 'games';

    protected $fillable = [
        'player_one_id', 'player_two_id', 'player_one_points', 'player_two_points'
    ];

   

}
