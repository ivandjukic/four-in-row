<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PlayerGame extends Authenticatable
{
    use Notifiable;

    protected $table = 'player_game';

    protected $fillable = [
        'player_id', 'game_id', 'points'
    ];

    public function player()
    {
        return $this->hasOne('App\Models\Player', 'id', 'player_id');
    }

    public function game()
    {
        return $this->hasOne('App\Models\Game', 'id', 'game_id');
    }
}
