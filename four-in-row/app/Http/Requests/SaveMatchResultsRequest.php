<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveMatchResultsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:games',
            'player_one_email' => 'required|email|exists:players,email',
            'player_one_points' => 'required|numeric|in:0,1,3',
            'player_two_email' => 'required|email|exists:players,email|different:player_one_email',
            'player_two_points' => 'required|numeric|in:0,1,3',
        ];
    }
}
