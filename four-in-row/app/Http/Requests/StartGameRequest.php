<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StartGameRequest extends FormRequest
{

     /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'player_one_name' => 'required',
            'player_one_email' => 'required|email',
            'player_two_name' => 'required',
            'player_two_email' => 'required|email|different:player_one_email',
            'game_width' => 'numeric|min:0|max:15',
            'game_height' => 'numeric|min:0|max:15'
        ];
    }

}
