<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Services\PlayerService;
use App\Services\PlayerGameService;



class PlayerController extends BaseController
{
    public function getPlayerData(Request $request) {
        $player_id = $request->get('player_id');
        $player = PLayerService::getPlayerbyId($player_id);
        if(!$player) {
            return response()->json([
                'success' => false,
                'error' => 'player_not_found',
                'data' => []  
            ]);
        }
        $player_games = PlayerGameService::getPlayerGames($player_id);
        $response = array();
        $response['name'] = $player->name;
        $response['email'] = $player->email;
        foreach ($player_games as $game) {
           $response['games'][] = array(
               'points' => $game->points,
               'date' => $game->created_at->format('d.m.Y H:i:s')
           );
        } 

        return response()->json([
            'success' => true,
            'error' => [],
            'data' => $response  
        ]);
    }

}