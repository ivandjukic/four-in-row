<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Http\Requests\StartGameRequest;
use App\Http\Requests\SaveMatchResultsRequest;
use App\Services\GameService;
use App\Models\Game;
use App\Services\PlayerGameService;


class GameController extends BaseController
{

    public function startNew(StartGameRequest $request) {
        $game_data = $request->validated();
        
        $game = GameService::createNewGame($game_data);
        if(!$game) {
            return response()->json([
                'success' => false,
                'error' => [
                    'Unexpected error has ocurred'
                ],
                'data' => []    
            ]);
        }
        return response()->json([
            'success' => true,
            'error' => [],
            'data' => json_decode($game)  
        ]);
    }

    public function saveMatchResults(SaveMatchResultsRequest $request) {
        $game_data = $request->validated();

        $game = Game::find($game_data['id']);
        if(!$game) {
            return response()->json([
                'success' => false,
                'error' => [
                    'Unexpected error has ocurred'
                ],
                'data' => []    
            ]); 
        }

        $game->player_one_points = $game_data['player_one_points'];
        $game->player_two_points = $game_data['player_two_points'];
        $game->save();
        PlayerGameService::savePlayerGame($game_data['player_one_email'], $game, $game_data['player_one_points']);
        PlayerGameService::savePlayerGame($game_data['player_two_email'], $game, $game_data['player_two_points']);
        
        return response()->json([
            'success' => true,
            'error' => [],
            'data' => 'game_results_saved'  
        ]);
    }
    
}
