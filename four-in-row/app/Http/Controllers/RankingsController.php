<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\Game;
use App\Models\PlayerGame;


class RankingsController extends BaseController
{
    public function getRankings(Request $request) {
        $from = $request->get('from');
        $to = $request->get('to');
        $query = PlayerGame::query();
        $query->select('player_id', \DB::raw('sum(points) as points'), \DB::raw('count(*) as games'));
        if($from) {
            $query->where('created_at', '>', $from);
        }
        if($to) {
            $query->where('created_at', '<', $to);
        }
        $query->groupBy(\DB::raw('player_id') );
        $results = $query->get();
        
        $response = array();
        foreach($results as $result) {
            $response[] = array(
                'player_id' => $result->player->id,
                'name' => $result->player->name,
                'email' => $result->player->email,
                'games' => $result->games,
                'points' => $result->points,
            );
        }  
        usort($response, function ($a, $b) { 
            return $a['points'] < $b['points']; 
        });
        
        return response()->json([
            'success' => true,
            'error' => [],
            'data' => $response  
        ]);
    }

}