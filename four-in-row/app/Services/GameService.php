<?php

namespace App\Services;

use App\Services\PlayerService;
use App\Models\Game;
use Illuminate\Support\Facades\Log;

class GameService
{
    public static function createNewGame($game_data) {
        $player_one = PlayerService::getPlayerbyEmail($game_data['player_one_email']);
        if(!$player_one) {
            $player_one = PlayerService::createNewPlayer($game_data['player_one_email'], $game_data['player_one_name']);
        }
        $player_two = PlayerService::getPlayerbyEmail($game_data['player_two_email']);
        if(!$player_two) {
            $player_two =PlayerService::createNewPlayer($game_data['player_two_email'], $game_data['player_two_name']);
        }

        try { 
            $game = Game::create([
                'player_one_id' => $player_one->id,
                'player_two_id' => $player_one->id,
                'player_one_points' => 0,
                'player_two_points' => 0        
            ]);

            return $game;
        } catch (\Exception $e) {
            Log::error("File:" .$e->getFile() . ", line:" . $e->getLine() . ", error:" . $e->getMessage());
            return null;
        }
    }

}
