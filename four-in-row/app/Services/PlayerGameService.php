<?php

namespace App\Services;

use App\Models\PlayerGame;
use App\Services\PlayerService;
use Illuminate\Support\Facades\Log;

class PlayerGameService
{
    public static function savePlayerGame($player_email, $game, $points) {
        try {

            $player = PlayerService::getPlayerbyEmail($player_email);
            $player_game = PlayerGame::create([
                'player_id' => $player->id,
                'game_id' => $game->id,
                'points' => $points
            ]); 

            return $player_game;
        } catch(\Exception $e) {
            Log::error("File:" .$e->getFile() . ", line:" . $e->getLine() . ", error:" . $e->getMessage());
            return null;
        }
    }

    public static function getPlayerGames($player_id) {
        $player_games = PlayerGame::query()
            ->where('player_id', $player_id)
            ->get();
        return $player_games;
    }
}
