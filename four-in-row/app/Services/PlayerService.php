<?php

namespace App\Services;

use App\Models\Player;
use Illuminate\Support\Facades\Log;

class PlayerService
{
    public static function getPlayerbyEmail($email) {
        $player = Player::query()
            ->where('email', $email)
            ->first();
        return $player;
    }

    public static function getPlayerbyId($id) {
        $player = Player::find($id);
        return $player;
    }

    public static function createNewPlayer($email, $name) {
        try {
            $player = Player::create([
                'email' => $email,
                'name' => $name
            ]); 
            return $player;
        } catch(\Exception $e) {
            Log::error("File:" .$e->getFile() . ", line:" . $e->getLine() . ", error:" . $e->getMessage());
            return null;
        }
    }

}
